@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table border = "1">
        <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Email</td>
        <td>Roles</td>   
        </tr>
        @foreach ($users as $user)
        <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->roles }}</td>
        </tr>
        @endforeach
        </table>
        <br>
        @if(auth()->user()->roles =='admin')
        <div>
            <form method="post" action="/create" enctype="multipart">
                @csrf
              <input type="text" name="textfield">
            <button type="submit" name="submit">Notify</button>  
            </form>
            
        </div>
        @endif

                </div>

            </div>



        </div>
    </div>
        
</div>
@endsection
