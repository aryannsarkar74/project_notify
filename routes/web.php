<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Notifications\TaskCompleted;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	User::where('roles','user') -> first();
	User::find(1)->notify(new TaskCompleted);
    return view('welcome');
});

Route::get('markAsRead',function(){
    auth()->user()->unreadNotifications->markAsRead();
    return redirect()->back();
})->name('markRead');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('insert','FirstController@insertform');
Route::post('create','FirstController@insert');

Route::get('/home','FirstController@index');

